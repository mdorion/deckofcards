export interface ApiParams {
    httpmethod: string;
    endpoint: string;
    method: string;
    showError?: boolean;
    silent?:boolean;
    params?:any;
    showWaitingDialog?:boolean;
}
