import { trigger, animate, transition, style, query, stagger } from '@angular/animations';

export const listAnimation =
    trigger('listAnimation', [

        transition('* => *', [
            query(':leave', [
                // stagger(100, [
                    animate('0.1s', style({ opacity: 0 }))
                // ])
            ], {optional:true}),
            query(':enter', [
                style({ opacity: 0, transform: 'translateX(-40px)' }),
                stagger(25, [
                    animate('0.3s', style({ opacity: 1, transform: 'translateX(0px)' }))
                ])
            ], {optional:true})
        ])
    ]);
