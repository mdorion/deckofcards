import { NgModule } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconRegistry,
    MatIconModule,
    MatProgressSpinnerModule
} from '@angular/material';

@NgModule({
    imports: [
      MatButtonModule,
      MatCardModule,
      MatToolbarModule,
      MatIconModule,
      MatProgressSpinnerModule
    ],
    exports: [
      MatButtonModule,
      MatCardModule,
      MatToolbarModule,
      MatIconModule,
      MatProgressSpinnerModule
    ],
})
export class CustomMaterialModuleModule {
    constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
        //Custom icons from https://materialdesignicons.com/
        // matIconRegistry.setDefaultFontSetClass
        matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
    }
}
