import { Suit } from './suit';
export class Card {
  icon:string;
  text:string;
  value:number;
  suit:Suit;

  constructor(card?: Card){
    if(card){
      this.text = (card.text) ? card.text : card.value.toString();
      this.value = card.value;
      this.suit = new Suit(card.suit.toString());
      this._setIcon();
    }
  }

  private _setIcon(){
    switch(this.text){
      case "J":{
        this.icon = "alpha-j";
        break;
      }
      case "Q":{
        this.icon = "alpha-q";
        break;
      }
      case "K":{
        this.icon = "alpha-k";
        break;
      }
      case "A":{
        this.icon = "alpha-a";
        break;
      }
      default:{
        /*10 doesnt have an icon, set icon to null*/
        this.icon = (this.value!==10) ? "numeric-"+this.value : null
      }
    }
  }
}
