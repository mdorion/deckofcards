export class Suit {
  icon:string;
  type:string;

  constructor(suit?:string){
    if(suit){
      this.type = suit;
      this._setIcon();
    }
  }

  private _setIcon(){
    switch(this.type){
      case "diamonds":{
        this.icon = "cards-diamond";
        break;
      }
      case "hearts":{
        this.icon = "cards-heart";
        break;
      }
      case "spades":{
        this.icon = "cards-spade";
        break;
      }
      case "clubs":{
        this.icon = "cards-club";
        break;
      }
    }
  }
}
