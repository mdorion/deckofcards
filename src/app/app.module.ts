import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

/*Angular Material needed elements*/
import { CustomMaterialModuleModule } from './modules/custom-material/custom-material-module.module';

/*Components*/
import { BodyComponent } from './components/layout/body/body.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { MainComponent } from './components/layout/main/main.component';
import { DeckComponent } from './components/cards/deck/deck.component';
import { SingleCardComponent } from './components/cards/single-card/single-card.component';
import { HandComponent } from './components/cards/hand/hand.component';

/*Services*/
import { DeckService } from './services/deck.service';
import { HandService } from './services/hand.service';

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    HeaderComponent,
    MainComponent,
    DeckComponent,
    SingleCardComponent,
    HandComponent
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModuleModule,
    HttpClientModule
  ],
  providers: [
    DeckService,
    HandService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
