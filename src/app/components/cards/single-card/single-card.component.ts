import { Component, OnInit, Input } from '@angular/core';
import { Card } from '@classes';

@Component({
  selector: 'app-single-card',
  templateUrl: './single-card.component.html',
  styleUrls: ['./single-card.component.scss']
})
export class SingleCardComponent implements OnInit {
  @Input() card:Card;
  constructor() { }

  ngOnInit() {
  }

  counter(i: number) {
    return new Array(i);
  }

}
