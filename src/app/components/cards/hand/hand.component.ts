import { Component, OnInit, Input } from '@angular/core';
import { listAnimation } from '@animations';
import { Card } from '@classes';

@Component({
  selector: 'app-hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.scss'],
  animations: [listAnimation]
})
export class HandComponent implements OnInit {
  @Input() cards:Card[];

  constructor() { }

  ngOnInit() {
  }

  calculTop(i){
    /*Static 8 card per row*/
    if(i>7){
      return -190+'px';
    }
  }

}
