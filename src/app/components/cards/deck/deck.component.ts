import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { listAnimation } from '@animations';
import { Card } from '@classes';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss'],
  animations: [listAnimation]
})
export class DeckComponent implements OnInit {
  @Input() cards: Card[];
  @Output() onShuffleDeck = new EventEmitter();
  @Output() onDealOneCard = new EventEmitter();
  @Output() onResetDeck = new EventEmitter();
  public showloading:boolean=true;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes:SimpleChanges){
    if(changes['cards']){
      if(this.cards.length > 0){
        this.showloading=false;
      }
    }
  }

  ShuffleDeck(){
    this.onShuffleDeck.emit();
  }

  DealOneCard(){
    this.onDealOneCard.emit();
  }

  ResetDeck(){
    this.onResetDeck.emit();
  }

}
