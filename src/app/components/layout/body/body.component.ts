import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from '@classes';
import { DeckService, HandService } from '@services';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit {
  public deckcards:Observable<Card[]>;
  public handcards:Observable<Card[]>;
  constructor(
    public deckService:DeckService,
    public handService:HandService
  ) { }

  ngOnInit() {
    this._getDeckCards();
    this._getHandCards();
  }

  private _getDeckCards(){
    this.deckcards = this.deckService.cards
  }

  private _getHandCards(){
    this.handcards = this.handService.cards
  }

  ShuffleDeck(){
    this.deckService.shuffle();
  }

  DealOneCard(){
    this.handService.addCard(this.deckService.dealOneCard());
  }

  ResetDeck(){
    this.deckService.reset();
    this.handService.reset();
  }

}
