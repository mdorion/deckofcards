import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  BehaviorSubject ,  of } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { Card } from '@classes';

@Injectable({
  providedIn: 'root'
})
export class DeckService {
  private _cards: BehaviorSubject<Card[]> = new BehaviorSubject(null);
  /*For a time question, I use my own datastore instead of @ngxs/store or @ngrx/store*/
  private dataStore:{
    originalDeck: Card[],
    cards: Card[],
  };
  constructor(
    private httpClient: HttpClient,
  ) {
    this.dataStore = { cards: null, originalDeck: [] };
  }

  get cards():Observable<Card[]>{
    //always be sure that we have a cards list
    this.GetCards().pipe(take(1)).subscribe();
    //return Observable of _cards, those who subscribe will always be notified
    return this._cards.asObservable();
  }

  private GetCards(force:boolean=false): Observable<Card[]>{
    /*If there is no cards in datastore or we need a force GET*/
    if(!this.dataStore.cards || force){
      //Set datastore to an empty array to prevent duplicate calls
      this.dataStore.cards = [];
      this._cards.next(this.dataStore.cards);
      return this._getJSONdeck()
      .pipe(
        map(cards => (cards) ? cards.map(card => new Card(card)) : null),
        tap(cards =>{
          /*Clone object to prevent to be modified*/
          this.dataStore.originalDeck = Object.assign([], cards);
          //Set cards in dataStore
          this.dataStore.cards = cards;

          //Submit changes to BehaviorSubject
          this._cards.next(this.dataStore.cards);
        })
      )

    }
    //Return cards from datastore
    return of(this.dataStore.cards);
  }

  private _getJSONdeck() : Observable<any>{
    /*Normally, here I would use another service that makes all get, post, update, delete request
    that handle errors, response, messages and http codes, but for a time question I go straight forward for the call
    */
    return this.httpClient.get(
      `/assets/cards-deck.json`,
    ).pipe(
      map(data => (data) ? data["cards"] : null),
    )
  }

  public reset(){
    this.dataStore.cards = Object.assign([], this.dataStore.originalDeck);
    this._cards.next(this.dataStore.cards);
  }

  public shuffle(){
    /*copy datastore cards object*/
    let cards = Object.assign([], this.dataStore.cards);
    cards.forEach((card, index) =>{
      /*remove one card at specified index*/
      let removedCard = cards.splice(index,1)[0];
      /*get random position for card*/
      let newPosition = this._getRandomInt(cards.length-1);
      /*place removedCard at new position in deck*/
      cards.splice(newPosition,0,removedCard);
    })
    /*Update datastore*/
    this.dataStore.cards = Object.assign([], cards);
    /*Submit change to BehaviorSubject*/
    this._cards.next(this.dataStore.cards);
  }

  public dealOneCard():Card{
    if(this.dataStore.cards.length>0){
      let card = this.dataStore.cards.pop();
      // this.dataStore.cards = this.dataStore.cards.slice(0,1);
      this._cards.next(this.dataStore.cards);
      return card;
    }else{
      //TODO error message no more card to deal
    }
  }

  private _getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}
