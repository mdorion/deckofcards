import { Injectable } from '@angular/core';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Card } from '@classes';

@Injectable({
  providedIn: 'root'
})
export class HandService {
  private _cards: BehaviorSubject<Card[]> = new BehaviorSubject([]);
  /*For a time question, I use my own datastore instead of @ngxs/store or @ngrx/store*/
  private dataStore:{
    cards: Card[],
  };
  constructor() {
    this.dataStore = { cards: [] };
  }

  get cards():Observable<Card[]>{
    return this._cards.asObservable();
  }

  addCard(card:Card){
    if(card){
      this.dataStore.cards.push(card);
      this._cards.next(this.dataStore.cards);
    }
  }

  reset(){
    this.dataStore.cards = [];
    this._cards.next(this.dataStore.cards);
  }
}
